﻿using Windows.UI.Xaml.Controls;
using WorkTime.PresentationLogic.ViewModels.LogWork;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace WorkTime.Pages
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class LogWorkPage : Page
    {
        public LogWorkPage(LogWorkViewModel vm)
        {
            this.InitializeComponent();
            this.DataContext = vm;
        }
    }
}
