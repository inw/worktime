﻿using Windows.UI.Xaml.Controls;
using WorkTime.PresentationLogic.ViewModels;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace WorkTime.Pages
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {

        public MainPage(MainPageViewModel vm)
        {
            this.InitializeComponent();
            this.DataContext = vm;
        }

        private void NavigationView_SelectionChanged(NavigationView sender, NavigationViewSelectionChangedEventArgs args)
        {
            var vm = ((MainPageViewModel)DataContext);
            vm.IsSettingsSelected = args.IsSettingsSelected;
            vm.SelectedItem = (NavigationViewItem)args.SelectedItem;
        }
    }
}
