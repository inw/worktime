﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.Globalization;
using Windows.System.UserProfile;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using WorkTime.DataAccess;
using WorkTime.DataAccess.Services;
using WorkTime.Domain.CrossCuttingConcerns;
using WorkTime.Domain.Repository;
using WorkTime.Domain.Services;
using WorkTime.Pages;
using WorkTime.PresentationLogic.ErrorHandler;
using WorkTime.PresentationLogic.Navigation;
using WorkTime.PresentationLogic.ParameterObjects;
using WorkTime.PresentationLogic.ViewModels;
using WorkTime.PresentationLogic.ViewModels.LogWork;
using WorkTime.PresentationLogic.ViewModels.LogWork.Regions;
using static WorkTime.Domain.CrossCuttingConcerns.Sync.AsyncHelpers;

namespace WorkTime
{
    /// <summary>
    /// Provides application-specific behavior to supplement the default Application class.
    /// </summary>
    sealed partial class App : Application, INavigationService
    {
        private readonly INavigationService _navigationService;
        private readonly DialogErrorHandler _errorHandler;
        private readonly ITimeProvider _clock;
        private readonly UserInfo _userInfo;
        private readonly IWorkDayRepository _repository;
        private readonly System.DayOfWeek _startDayOfWeek;
        private readonly ICurrentWorkWeekService _currentWorkWeekService;

        private Frame _rootFrame;
        private Frame _contentFrame;

        /// <summary>
        /// Initializes the singleton application object.  This is the first line of authored code
        /// executed, and as such is the logical equivalent of main() or WinMain().
        /// </summary>
        public App()
        {
            InitializeComponent();
            Suspending += OnSuspending;

            _navigationService = this;
            _errorHandler = new DialogErrorHandler();
            _clock = new TimeProvider();
            _userInfo = new UserInfo(startDayOfWeek: System.DayOfWeek.Monday);
            _repository = new DiskWorkDayRepository(_clock);
            _currentWorkWeekService = new DiskCurrentWorkWeekService(_clock, _userInfo.StartDayOfWeek, _repository);
        }

        /// <summary>
        /// Invoked when the application is launched normally by the end user.  Other entry points
        /// will be used such as when the application is launched to open a specific file.
        /// </summary>
        /// <param name="e">Details about the launch request and process.</param>
        protected override void OnLaunched(LaunchActivatedEventArgs e)
        {
            ApplicationLanguages.PrimaryLanguageOverride = GlobalizationPreferences.Languages[0];

            if (Window.Current.Content == null)
            {
                Window.Current.Content = new Frame();
                Window.Current.Activate();

                RunSync(() => NavigateTo<MainPageViewModel>(new Func<Task>(async () => await NavigateTo<LogWorkViewModel>())));
            }
        }

        public async Task NavigateTo<TViewModel>(Func<Task> whenDone = null, object model = null)
            where TViewModel : IViewModel
        {
            Page page = CreatePage(typeof(TViewModel));
            var viewModel = (IViewModel)page.DataContext;

            await viewModel.Initialize(whenDone, model);

            if (viewModel is MainPageViewModel)
            {
                if (_rootFrame == null)
                    InitializeMainPage();
                _rootFrame.Content = page;
            }
            else
            {
                if (_rootFrame == null)
                    return;

                if (_contentFrame == null)
                    SetupContentFrame();

                _contentFrame.Content = page;
            }
            if (whenDone != null)
                await whenDone.Invoke();
        }

        private void SetupContentFrame()
        {
            Page rootPage = (Page)_rootFrame.Content;
            _contentFrame = (Frame)rootPage?.FindName("contentFrame") ?? throw new InvalidOperationException("Content frame could not be retrieved.");
        }

        private void InitializeMainPage()
        {
            _rootFrame = (Frame)Window.Current.Content;
        }

        private Page CreatePage(Type viewModelType)
        {
            var pageSwitch = new Dictionary<Type, Func<Page>>
            {
                { typeof(LogWorkViewModel), CreateLogWorkPage },
                { typeof(MainPageViewModel), CreateMainPage },
                { typeof(SettingsViewModel), CreateSettingsPage }
            };

            if (pageSwitch.TryGetValue(viewModelType, out Func<Page> newPage))
                return newPage();
            else
                throw new InvalidOperationException($"ViewModel {viewModelType} not found.");
        }

        private Page CreateSettingsPage()
        {
            return new SettingsPage(
                new SettingsViewModel(_repository as IDiskStorage));
        }

        private Page CreateMainPage()
        {
            return new MainPage(
                new MainPageViewModel(_navigationService, _errorHandler));
        }

        private Page CreateLogWorkPage()
        {
            var logWorkInfastructure = new LogWorkInfrastructure(
                clock: _clock,
                workDayRepository: _repository,
                currentWeekService: _currentWorkWeekService,
                errorHandler: _errorHandler);                
            var regionModels = new LogWorkViewModelRegions(
                new CurrentWeekRegionViewModel(_userInfo.StartDayOfWeek),
                new CurrentWorkDayRegionViewModel(logWorkInfastructure)
                );

            return new LogWorkPage(
                new LogWorkViewModel(
                    _currentWorkWeekService,
                    _repository,
                    _userInfo,
                    regionModels));
        }

        /// <summary>
        /// Invoked when Navigation to a certain page fails
        /// </summary>
        /// <param name="sender">The Frame which failed navigation</param>
        /// <param name="e">Details about the navigation failure</param>
        void OnNavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            throw new InvalidOperationException("Failed to load Page " + e.SourcePageType.FullName);
        }

        /// <summary>
        /// Invoked when application execution is being suspended.  Application state is saved
        /// without knowing whether the application will be terminated or resumed with the contents
        /// of memory still intact.
        /// </summary>
        /// <param name="sender">The source of the suspend request.</param>
        /// <param name="e">Details about the suspend request.</param>
        private void OnSuspending(object sender, SuspendingEventArgs e)
        {
            var deferral = e.SuspendingOperation.GetDeferral();
            //TODO: Save application state and stop any background activity
            deferral.Complete();
        }
    }
}
