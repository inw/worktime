﻿using System;
using System.Windows.Input;

namespace WorkTime.PresentationLogic.Commands
{
    public class RelayCommand : ICommand
    {
        private readonly Predicate<object> _canExecute;
        private readonly Action<object> _executeAction;


        private bool _canBeExecuted = true;

        public event EventHandler CanExecuteChanged;

        public RelayCommand(Action<object> execute)
        {
            _executeAction = execute;
            _canExecute = parameter => true;
        }

        public RelayCommand(Action execute)
        {
            _canExecute = parameter => true;
            _executeAction = parameter => execute();
        }

        public RelayCommand(Func<bool> canExecute, Action execute)
        {
            _canExecute = parameter => canExecute();
            _executeAction = parameter => execute();

        }

        public bool CanExecute(object parameter)
        {
            var lastState = _canBeExecuted;
            _canBeExecuted = _canExecute(parameter);

            if (lastState != _canBeExecuted)
                CanExecuteChanged?.Invoke(this, EventArgs.Empty);

            return _canBeExecuted;
        }

        public void Execute(object parameter)
            => _executeAction(parameter);
    }
}
