﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkTime.PresentationLogic.ViewModels;
using WorkTime.PresentationLogic.ViewModels.LogWork.Regions;

namespace WorkTime.PresentationLogic.ParameterObjects
{
    public class LogWorkViewModelRegions
    {
        public CurrentWeekRegionViewModel CurrentWeekRegion { get; }
        public CurrentWorkDayRegionViewModel CurrentWorkDayRegion { get; }

        public LogWorkViewModelRegions(
            CurrentWeekRegionViewModel currentWeekViewModel,
            CurrentWorkDayRegionViewModel currentWorkDayViewModel)
        {
            CurrentWeekRegion = currentWeekViewModel ?? throw new ArgumentNullException(nameof(currentWeekViewModel));
            CurrentWorkDayRegion = currentWorkDayViewModel ?? throw new ArgumentNullException(nameof(currentWorkDayViewModel));
        }
    }
}
