﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkTime.PresentationLogic.ParameterObjects
{
    public class UserInfo
    {
        public System.DayOfWeek StartDayOfWeek { get; }
        public UserInfo(DayOfWeek startDayOfWeek)
        {
            StartDayOfWeek = startDayOfWeek;
        }
    }
}
