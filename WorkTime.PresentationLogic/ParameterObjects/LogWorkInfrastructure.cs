﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkTime.Domain.CrossCuttingConcerns;
using WorkTime.Domain.CrossCuttingConcerns.Sync;
using WorkTime.Domain.Repository;
using WorkTime.Domain.Services;

namespace WorkTime.PresentationLogic.ParameterObjects
{
    public class LogWorkInfrastructure
    {
        public ITimeProvider Clock { get; }
        public IWorkDayRepository WorkDayRepository { get; }
        public IErrorHandler ErrorHandler { get; }

        public LogWorkInfrastructure(ITimeProvider clock, IWorkDayRepository workDayRepository, ICurrentWorkWeekService currentWeekService, IErrorHandler errorHandler)
        {
            Clock = clock ?? throw new ArgumentNullException(nameof(clock));
            WorkDayRepository = workDayRepository ?? throw new ArgumentNullException(nameof(workDayRepository));
            ErrorHandler = errorHandler ?? throw new ArgumentNullException(nameof(errorHandler));
        }
    }
}
