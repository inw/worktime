﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Popups;
using WorkTime.Domain.CrossCuttingConcerns.Sync;
using static WorkTime.Domain.CrossCuttingConcerns.Sync.AsyncHelpers;

namespace WorkTime.PresentationLogic.ErrorHandler
{
    public class DialogErrorHandler : IErrorHandler
    {
#pragma warning disable S3168 // "async" methods should not return "void"
        public async Task HandleException(Exception ex)
#pragma warning restore S3168 // "async" methods should not return "void"
        {
            var dialog = new MessageDialog(ex.ToString(), "Application Error!");
            await dialog.ShowAsync();
        }
    }
}
