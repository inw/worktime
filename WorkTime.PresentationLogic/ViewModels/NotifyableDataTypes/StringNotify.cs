﻿using System.ComponentModel;

namespace WorkTime.PresentationLogic.ViewModels.NotifyableDataTypes
{
    public class StringNotify : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private string _value;
        public string Value
        {
            get
            {
                return _value;
            }
            set
            {
                _value = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Value)));
            }
        }

        public StringNotify(string value)
        {
            Value = value;
        }
    }

}
