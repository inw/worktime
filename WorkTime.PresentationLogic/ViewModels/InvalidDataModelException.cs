﻿using System;
using System.Runtime.Serialization;

namespace WorkTime.PresentationLogic.ViewModels
{
    [Serializable]
    public class InvalidDataModelException : Exception
    {
        public InvalidDataModelException()
        {
        }

        public InvalidDataModelException(string message) : base(message)
        {
        }

        public InvalidDataModelException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected InvalidDataModelException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
