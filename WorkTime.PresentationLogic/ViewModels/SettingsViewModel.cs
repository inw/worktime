﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.ApplicationModel.Resources;
using Windows.System;
using Windows.UI.Xaml;
using WorkTime.Domain.Repository;
using WorkTime.PresentationLogic.Commands;
using static WorkTime.Domain.CrossCuttingConcerns.Sync.AsyncHelpers;

namespace WorkTime.PresentationLogic.ViewModels
{
    public class SettingsViewModel : IViewModel, INotifyPropertyChanged
    {
        private readonly IDiskStorage _diskStorage;
        private readonly ResourceLoader _resourceLoader;

        public ICommand OpenSaveFolderCommand { get; private set; }
        public string Title { get; private set; }
        public string SaveOptionsTitle { get; private set; }
        public string SaveFolderCaption { get; private set; }
        public int CaptionMaxWidth { get; private set; }

        public string SaveFolderPath { get; private set; }

        public Visibility SaveOptionsVisibility { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public SettingsViewModel(IDiskStorage diskStorage)
        {
            _diskStorage = diskStorage;
            _resourceLoader = ResourceLoader.GetForCurrentView();
            OpenSaveFolderCommand = new RelayCommand(OpenSaveFolder);
        }

#pragma warning disable S3168 // "async" methods should not return "void"
        private async void OpenSaveFolder(object obj)
#pragma warning restore S3168 // "async" methods should not return "void"
        {
            try
            {
                await Launcher.LaunchFolderPathAsync(SaveFolderPath);
            }
            catch (Exception ex)
            {

            }
        }

        public async Task Initialize(Func<Task> whenDone, object model)
        {
            Title = _resourceLoader.GetString("Settings/Title");
            InvokePropertyChanged(nameof(Title));
            SetupSaveOptions();
            
            await InvokeAsync(whenDone);

            await Task.CompletedTask;
        }

        private void InvokePropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }

        private void SetupSaveOptions()
        {
            SaveOptionsTitle = _resourceLoader.GetString("Settings/SaveOptionsTitle");
            InvokePropertyChanged(nameof(SaveOptionsTitle));
            SaveFolderCaption = _resourceLoader.GetString("Settings/SaveFolderCaption");
            InvokePropertyChanged(nameof(SaveFolderCaption));

            SaveOptionsVisibility = _diskStorage == null ? Visibility.Collapsed : Visibility.Visible;
            InvokePropertyChanged(nameof(Title));

            if (_diskStorage == null)
                return;

            SaveFolderPath = _diskStorage.StoragePath;
            InvokePropertyChanged(nameof(SaveFolderPath));
        }
    }
}
