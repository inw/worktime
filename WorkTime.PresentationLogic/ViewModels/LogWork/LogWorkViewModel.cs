﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;
using WorkTime.DataAccess.Exceptions;
using WorkTime.Domain.Repository;
using WorkTime.Domain.Services;
using WorkTime.Domain.WorkDay;
using WorkTime.PresentationLogic.ParameterObjects;
using WorkTime.PresentationLogic.ViewModels.LogWork.Regions;
using static WorkTime.Domain.CrossCuttingConcerns.Sync.AsyncHelpers;

namespace WorkTime.PresentationLogic.ViewModels.LogWork
{
    public class LogWorkViewModel : IViewModel
    {

        private readonly ICurrentWorkWeekService _currentWeekService;
        private readonly IWorkDayRepository _repository;

        public CurrentWeekRegionViewModel CurrentWeek { get; private set; }
        public CurrentWorkDayRegionViewModel CurrentWorkDay { get; private set; }

        public LogWorkViewModel(
            ICurrentWorkWeekService currentWeekService,
            IWorkDayRepository repository,
            UserInfo userInfo,
            LogWorkViewModelRegions regionViewModels)
        {
            _currentWeekService = currentWeekService ?? throw new ArgumentNullException(nameof(currentWeekService));
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
            CurrentWorkDay = regionViewModels.CurrentWorkDayRegion;
            CurrentWeek = regionViewModels.CurrentWeekRegion;

            CurrentWorkDay.PropertyChanged += OnCurrentWorkDayPropertyChanged;
        }

        private void OnCurrentWorkDayPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "CurrentWorkDayData":
                    RunSync(LoadCurrentWeekModel);
                    break;

                default:
                    break;
            }
        }

        public async Task Initialize(Func<Task> whenDone = null, object model = null)
        {
            await InitializeRegionViewModels();
            await LoadDataIntoViewModels();
            
            await InvokeAsync(whenDone);
        }
        

        private async Task InitializeRegionViewModels()
        {
            await CurrentWeek.Initialize();
            await CurrentWorkDay.Initialize();
        }

        private async Task LoadDataIntoViewModels()
        {
            await LoadCurrentWorkDayData();
            await LoadCurrentWeekModel();
        }

        private async Task LoadCurrentWeekModel()
        {
            var currentWeekData = await _currentWeekService.GetCurrentWeek();
            CurrentWeek.Update(currentWeekData);
        }


        private async Task LoadCurrentWorkDayData()
        {
            var data = new WorkDayModel(await _repository.GetCurrent())
                ?? throw new DataAccessException($"WorkDayModel could not be retrieved.");
            CurrentWorkDay.Update(data);
        }
    }
}
