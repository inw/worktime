﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Threading.Tasks;
using WorkTime.Domain.WorkWeek;
using WorkTime.PresentationLogic.ViewModels.NotifyableDataTypes;
using static WorkTime.Domain.CrossCuttingConcerns.Sync.AsyncHelpers;

namespace WorkTime.PresentationLogic.ViewModels.LogWork.Regions
{
    public class CurrentWeekRegionViewModel : IViewModel, INotifyPropertyChanged
    {
        private readonly DayOfWeek _startDayOfWeek;
        private readonly List<DayOfWeek> _dayOfWeeks;

        public ObservableCollection<StringNotify> WeekDayNames { get; set; }
        public ObservableCollection<StringNotify> WeekDayLoggedWork { get; set; }
        public string TotalTime { get; private set; }

        public CurrentWeekRegionViewModel(DayOfWeek startDayOfWeek)
        {
            _startDayOfWeek = startDayOfWeek;
            _dayOfWeeks = new List<DayOfWeek>();

            WeekDayNames = new ObservableCollection<StringNotify>();
            WeekDayLoggedWork = new ObservableCollection<StringNotify>();
        }

        private void SetupEmptyWorkLogged()
        {
            for (int i = 0; i < 7; i++)
                WeekDayLoggedWork.Add(new StringNotify(string.Empty));
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void Update(IWorkWeekModel currentWeek)
        {

            TimeSpan total = new TimeSpan();
            int dayIndex = 0;
            foreach (var day in _dayOfWeeks)
            {
                string value = string.Empty;
                if (currentWeek.LoggedWorkByDay.TryGetValue(day, out TimeSpan loggedWork))
                {
                    total = total.Add(loggedWork);
                    value = loggedWork.ToString(@"hh\:mm");
                }

                WeekDayLoggedWork[dayIndex].Value = value;
                dayIndex++;
            }
            TotalTime = total.ToString(@"hh\:mm");

            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(TotalTime)));
        }

        private void SetupWeekDayOrder()
        {
            int[] days = CalculateDayOfWeekOrder();
            foreach (var weekdayNumber in days)
            {
                var currentDayOfWeek = (DayOfWeek)weekdayNumber;
                _dayOfWeeks.Add(currentDayOfWeek);
                WeekDayNames.Add(new StringNotify(DateTimeFormatInfo.CurrentInfo.GetDayName(currentDayOfWeek)));
            }
        }

        private int[] CalculateDayOfWeekOrder()
        {
            int[] days = new int[7];
            int startDay = (int)_startDayOfWeek;
            days[0] = startDay;
            for (int i = 1; i < 7; i++)
            {
                int day = startDay + i;
                if (day > 6)
                    day = day - 7;

                days[i] = day;
            }

            return days;
        }

        public async Task Initialize(Func<Task> whenDone = null, object model = null)
        {
            SetupWeekDayOrder();
            SetupEmptyWorkLogged();

            await InvokeAsync(whenDone);

            await Task.CompletedTask;
        }
    }
}
