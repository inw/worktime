﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.ApplicationModel.Resources;
using WorkTime.Domain.CrossCuttingConcerns;
using WorkTime.Domain.DTO;
using WorkTime.Domain.Repository;
using WorkTime.Domain.WorkDay;
using WorkTime.PresentationLogic.Commands;
using WorkTime.PresentationLogic.ParameterObjects;
using static WorkTime.Domain.CrossCuttingConcerns.Sync.AsyncHelpers;

namespace WorkTime.PresentationLogic.ViewModels.LogWork.Regions
{
    public class CurrentWorkDayRegionViewModel : IViewModel, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private const string TIME_DISPLAY_FORMAT = "t";
        private const string DATE_DISPLAY_FORMAT = "D";

        private readonly ITimeProvider _clock;
        private readonly IWorkDayRepository _repository;        
        private readonly ResourceLoader _resourceLoader;

        public ICommand ToggleWorkCommand { get; }
        public IWorkDayData CurrentWorkDayData { get; private set; }

        public string ToggleWorkButtonText { get; private set; }
        public string WorkStartedText { get; private set; }
        public string WorkStoppedText { get; private set; }
        public string Title { get; private set; }
        public string CurrentDayText { get; private set; }
        public string WorkTotalText { get; private set; }

        public CurrentWorkDayRegionViewModel(LogWorkInfrastructure infrastruct)
        {
            _clock = infrastruct.Clock ?? throw new NullReferenceException(nameof(infrastruct.Clock));
            _repository = infrastruct.WorkDayRepository ?? throw new NullReferenceException(nameof(infrastruct.WorkDayRepository));
            _resourceLoader = ResourceLoader.GetForCurrentView();
            
            ToggleWorkCommand = new RelayCommand(ToggleWork);
        }

        public async Task Initialize(Func<Task> whenDone = null, object model = null)
        {
            await InvokeAsync(whenDone);

            await Task.CompletedTask;
        }

        private void SetTitle()
        {
            Title = string.Format(_resourceLoader.GetString("LogWork/Title"));
            CurrentDayText = CurrentWorkDayData.Date.ToString(DATE_DISPLAY_FORMAT);
            PropertyChanged.Invoke(this, new PropertyChangedEventArgs(nameof(Title)));
            PropertyChanged.Invoke(this, new PropertyChangedEventArgs(nameof(CurrentDayText)));
        }

        private void SetupWorkButtonText()
        {
            if (CurrentWorkDayData.CurrentlyWorking)
                ToggleWorkButtonText = _resourceLoader.GetString("Button/Caption/StopWork");
            else
            {
                ToggleWorkButtonText = _resourceLoader.GetString("Button/Caption/StartWork");
                SetWorkTotalText();
            }

            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(ToggleWorkButtonText)));
        }

        private void SetWorkTotalText()
        {
            var workClocked = CurrentWorkDayData.LoggedWork;
            if (workClocked.TotalMinutes == 0)
                WorkTotalText = _resourceLoader.GetString("LogWork/WorkTotal/Empty");
            else
                WorkTotalText = string.Format(_resourceLoader.GetString("LogWork/WorkTotalText"),
                    workClocked.Hours, workClocked.Minutes);

            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(WorkTotalText)));
        }

        private void SetupDefaultText()
        {
            var lastStart = CurrentWorkDayData.WorkStartTimeStamps.LastOrDefault();
            var lastStop = CurrentWorkDayData.WorkStopTimeStamps.LastOrDefault();

            SetWorkStartedText(lastStart);
            SetWorkStoppedText(lastStop);
            SetWorkTotalText();

            SetupWorkButtonText();
        }


        private void SetWorkStoppedText(DateTimeOffset stop)
        {
            if (stop == default)
                WorkStoppedText = _resourceLoader.GetString("LogWork/WorkEnded/Empty");
            else if (CurrentWorkDayData.CurrentlyWorking)
                WorkStoppedText = string.Format(_resourceLoader.GetString("LogWork/LastBreakText"),
                    stop.ToString(TIME_DISPLAY_FORMAT));
            else
                WorkStoppedText = string.Format(_resourceLoader.GetString("LogWork/WorkEndedText"),
                    stop.ToString(TIME_DISPLAY_FORMAT));

            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(WorkStoppedText)));
        }

        private void SetWorkStartedText(DateTimeOffset start)
        {
            if (start == default)
                WorkStartedText = _resourceLoader.GetString("LogWork/WorkStarted/Empty");
            else
                WorkStartedText = string.Format(_resourceLoader.GetString("LogWork/WorkStartedText"),
                    start.ToString(TIME_DISPLAY_FORMAT));
            SetWorkStoppedText(CurrentWorkDayData.WorkStopTimeStamps.LastOrDefault());

            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(WorkStartedText)));
        }

        private void ToggleWork()
        {
            if (CurrentWorkDayData is IWorkDayModel model)
            {
                AddTimeStamp(model);
                SaveCurrentWorkdayData();
            }
            else
            {
                throw new InvalidDataModelException($"{nameof(CurrentWorkDayData)} needs to implement IWorkDayModel");
            }
        }

        private void SaveCurrentWorkdayData()
        {
            _repository.Insert(CurrentWorkDayData);
        }

        private void AddTimeStamp(IWorkDayModel model)
        {
            if (CurrentWorkDayData.CurrentlyWorking)
                CurrentWorkDayData = model.AddTimeStamp(new StopWorkTimeStamp(_clock.Now));
            else
                CurrentWorkDayData = model.AddTimeStamp(new StartWorkTimeStamp(_clock.Now));

            UpdateText();

            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(CurrentWorkDayData)));
        }

        internal void Update(IWorkDayData workDay)
        {
            CurrentWorkDayData = workDay ?? throw new ArgumentNullException(nameof(workDay));
            UpdateText();
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(CurrentWorkDayData)));
        }


        private void UpdateText()
        {
            SetTitle();
            SetupDefaultText();
        }
    }
}
