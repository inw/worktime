﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using WorkTime.Domain.CrossCuttingConcerns.Sync;
using WorkTime.PresentationLogic.Navigation;
using WorkTime.PresentationLogic.ViewModels.LogWork;
using static WorkTime.Domain.CrossCuttingConcerns.Sync.AsyncHelpers;

namespace WorkTime.PresentationLogic.ViewModels
{
    public class MainPageViewModel : IViewModel, INotifyPropertyChanged
    {
        private readonly INavigationService _navigationService;
        private readonly IErrorHandler _errorHandler;
        private NavigationViewItem _currentSelection;
        public List<NavigationViewItem> NavItems { get; private set; }
        public NavigationViewItem SelectedItem
        {
            get { return _currentSelection; }
            set
            {
                _currentSelection = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(SelectedItem)));
            }
        }

        public bool IsSettingsSelected { get; set; }

        public MainPageViewModel(INavigationService navigationService, IErrorHandler errorHandler)
        {
            _navigationService = navigationService ?? throw new ArgumentNullException(nameof(navigationService));
            _errorHandler = errorHandler ?? throw new ArgumentNullException(nameof(errorHandler));
            NavItems = new List<NavigationViewItem>();
            PropertyChanged += OnPropertyChanged;
        }

        private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case nameof(SelectedItem):
                    LoadPage().AsAsyncVoidWithExceptionHandler(_errorHandler);
                    break;

                default:
                    break;
            }
        }

        private async Task LoadPage()
        {
            if (IsSettingsSelected)
                await _navigationService.NavigateTo<SettingsViewModel>();

            if (SelectedItem.Tag is Func<Task> nav)
                await nav.Invoke();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void AddItems()
        {
            NavItems.Add(new NavigationViewItem
            {
                Content = "Log Work",
                Icon = new SymbolIcon(Symbol.Manage),
                Tag = new Func<Task>(async () => await _navigationService.NavigateTo<LogWorkViewModel>())
            });
        }

        public async Task Initialize(Func<Task> whenDone = null, object model = null)
        {
            AddItems();
            InvokePropertyChanged(nameof(NavItems));
            SetFirstSelection();

            await InvokeAsync(whenDone);
            await Task.CompletedTask;
        }

        private void SetFirstSelection()
        {
            SelectedItem = NavItems.FirstOrDefault();
            SelectedItem.IsSelected = true;
            InvokePropertyChanged(nameof(SelectedItem));
        }

        private void InvokePropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
    }
}
