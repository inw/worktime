﻿using System;
using System.Threading.Tasks;

namespace WorkTime.PresentationLogic.ViewModels
{
    public interface IViewModel
    {
        Task Initialize(Func<Task> whenDone = null, object model = null);
    }
}
