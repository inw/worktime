﻿using System;
using System.Threading.Tasks;
using WorkTime.PresentationLogic.ViewModels;

namespace WorkTime.PresentationLogic.Navigation
{
    public interface INavigationService
    {
        Task NavigateTo<TViewModel>(Func<Task> whenDone = null, object model = null)
            where TViewModel : IViewModel;
    }
}
