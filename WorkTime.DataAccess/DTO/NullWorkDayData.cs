﻿using System;
using System.Collections.Generic;
using WorkTime.Domain.DTO;

namespace WorkTime.DataAccess.DTO
{
    public class NullWorkDayData : IWorkDayData, IWorkDaySummary
    {
        public bool CurrentlyWorking { get; }

        public IWorkTimeStamp LastTimeStamp { get; }

        public IEnumerable<DateTimeOffset> WorkStartTimeStamps { get; } = new DateTimeOffset[0];

        public IEnumerable<DateTimeOffset> WorkStopTimeStamps { get; } = new DateTimeOffset[0];

        public DateTimeOffset Date { get; }

        public TimeSpan LoggedWork { get; }

        public DayOfWeek WeekDay { get; }
    }
}
