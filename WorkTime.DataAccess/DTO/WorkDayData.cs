﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using WorkTime.DataAccess.Serialization.Json;
using WorkTime.Domain.DTO;

namespace WorkTime.DataAccess.DTO
{
    [JsonObject(MemberSerialization.OptIn)]
    internal class WorkDayData : IWorkDayData, IWorkDaySummary
    {
        [JsonProperty]
        public bool CurrentlyWorking { get; internal set; }

        [JsonConverter(typeof(WorkTimeStampConverter))]
        public IWorkTimeStamp LastTimeStamp { get; internal set; }
        [JsonProperty]
        public IEnumerable<DateTimeOffset> WorkStartTimeStamps { get; internal set; }
        [JsonProperty]
        public IEnumerable<DateTimeOffset> WorkStopTimeStamps { get; internal set; }
        [JsonProperty]
        public DateTimeOffset Date { get; internal set; }
        [JsonProperty]
        public TimeSpan LoggedWork { get; internal set; }

        public DayOfWeek WeekDay => Date.DayOfWeek;

        public WorkDayData(DateTimeOffset date)
        {
            WorkStartTimeStamps = new List<DateTimeOffset>();
            WorkStopTimeStamps = new List<DateTimeOffset>();
            Date = date;
        }

        [JsonConstructor]
        public WorkDayData(
            IEnumerable<DateTimeOffset> startStamps,
            IEnumerable<DateTimeOffset> stopStamps,
            IWorkTimeStamp lastTimeStamp,
            DateTimeOffset trackDate,
            TimeSpan clockedWork,
            bool isWorking)
        {
            LoggedWork = clockedWork;
            WorkStartTimeStamps = startStamps;
            WorkStopTimeStamps = stopStamps;
            Date = trackDate;
            LastTimeStamp = lastTimeStamp;
            CurrentlyWorking = isWorking;
        }

        public WorkDayData(IWorkDayData model)
        {
            LoggedWork = model.LoggedWork;
            WorkStartTimeStamps = model.WorkStartTimeStamps;
            WorkStopTimeStamps = model.WorkStopTimeStamps;
            Date = model.Date;
            LastTimeStamp = model.LastTimeStamp;
            CurrentlyWorking = model.CurrentlyWorking;
        }
    }
}
