﻿using System;
using System.Threading.Tasks;
using WorkTime.Domain.DTO;
using WorkTime.Domain.Repository;
using WorkTime.Domain.WorkDay;

namespace WorkTime.DataAccess
{
    public class FakeWorkDayRepository : IWorkDayRepository
    {
        IWorkDayData _current;

        public async Task Delete(IWorkDayData model)
        {
            throw new NotImplementedException();
        }

        public async Task<IWorkDayData> GetByDate(DateTimeOffset date)
        {
            throw new NotImplementedException();
        }

        public async Task<IWorkDayData> GetCurrent()
        {
            if (_current == null)
                _current = new WorkDayModel(DateTimeOffset.Now);

            return await Task.FromResult(_current);
        }

        public async Task Insert(IWorkDayData model)
        {
            _current = model;
            await Task.CompletedTask;
        }
    }
}
