﻿using System;
using System.Threading.Tasks;
using WorkTime.DataAccess.DTO;
using WorkTime.Domain.CrossCuttingConcerns;
using WorkTime.Domain.DTO;
using WorkTime.Domain.Repository;
using WorkTime.Domain.Services;
using WorkTime.Domain.WorkWeek;

namespace WorkTime.DataAccess.Services
{
    public class DiskCurrentWorkWeekService : ICurrentWorkWeekService
    {
        private readonly ITimeProvider _clock;
        private readonly DayOfWeek _startOfWeek;
        private readonly IWorkDayRepository _workDayRepository;

        public DiskCurrentWorkWeekService(ITimeProvider clock, DayOfWeek startOfWeek, IWorkDayRepository workDayRepo)
        {
            _clock = clock ?? throw new ArgumentNullException(nameof(clock));
            _startOfWeek = startOfWeek;
            _workDayRepository = workDayRepo ?? throw new ArgumentException(nameof(workDayRepo));
        }

        public async Task<IWorkWeekModel> GetCurrentWeek()
        {
            DateTimeOffset currentDay = _clock.Now.Date;
            var currentWeek = new CurrentWorkWeekModel(currentDay, _startOfWeek);
            for (int day = 0; day < 7; day++)
            {
                var workDay = await _workDayRepository.GetByDate(currentWeek.WeekStart.AddDays(day));
                if (workDay is NullWorkDayData)
                    continue;

                currentWeek.Add((IWorkDaySummary)workDay);
            }

            return currentWeek;
        }
    }
}
