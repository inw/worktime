﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using WorkTime.Domain.DTO;
using WorkTime.Domain.WorkDay;

namespace WorkTime.DataAccess.Serialization.Json
{
    internal class WorkTimeStampConverter : JsonConverter<IWorkTimeStamp>
    {
        public override void WriteJson(JsonWriter writer, IWorkTimeStamp value, JsonSerializer serializer)
        {
            writer.WriteStartObject();

            writer.WritePropertyName("Type");

            if (value is StartWorkTimeStamp)
                writer.WriteValue(nameof(StartWorkTimeStamp));
            else if (value is StopWorkTimeStamp)
                writer.WriteValue(nameof(StopWorkTimeStamp));
            else
                writer.WriteValue(string.Empty);

            writer.WritePropertyName(nameof(value.Timestamp));
            writer.WriteValue(value.Timestamp.ToString("o"));

            writer.WriteEndObject();
        }

        public override IWorkTimeStamp ReadJson(JsonReader reader, Type objectType, IWorkTimeStamp existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            Dictionary<string, object> propertValueTuple = new Dictionary<string, object>();

            var reading = true;
            string currentProperty = default;
            object currentValue = default;

            while (reading)
            {

                switch (reader.TokenType)
                {
                    case JsonToken.None:
                        break;
                    case JsonToken.StartObject:
                        NextElement(reader);
                        break;

                    case JsonToken.StartArray:
                        break;
                    case JsonToken.StartConstructor:
                        break;
                    case JsonToken.PropertyName:
                        currentProperty = reader.Value as string;
                        NextElement(reader);
                        break;
                    case JsonToken.Comment:
                        break;
                    case JsonToken.Raw:
                        break;
                    case JsonToken.Integer:
                        break;
                    case JsonToken.Float:
                        break;
                    case JsonToken.Date:
                        currentValue = new DateTimeOffset((DateTime)reader.Value);
                        NextElement(reader);
                        break;
                    case JsonToken.String:
                        currentValue = reader.Value as string;
                        NextElement(reader);
                        break;
                    case JsonToken.Boolean:
                        break;
                    case JsonToken.Null:
                        break;
                    case JsonToken.Undefined:
                        break;
                    case JsonToken.EndObject:
                        reading = false;
                        break;
                    case JsonToken.EndArray:
                        break;
                    case JsonToken.EndConstructor:
                        break;
                    case JsonToken.Bytes:
                        break;
                    default:
                        throw new InvalidOperationException();
                }

                if (currentProperty != default && currentValue != default)
                {
                    propertValueTuple.Add(currentProperty, currentValue);
                    currentValue = default;
                    currentProperty = default;
                }
            }

            if (!propertValueTuple.ContainsKey("Type") && !propertValueTuple.ContainsKey("Timestamp"))
                return new NullTimeStamp();
            else
                return CreateTimeStamp(propertValueTuple["Type"] as string, (DateTimeOffset)propertValueTuple["Timestamp"]);
        }

        private IWorkTimeStamp CreateTimeStamp(string type, DateTimeOffset timestamp)
        {
            if (type == nameof(StartWorkTimeStamp))
                return new StartWorkTimeStamp(timestamp);

            if (type == nameof(StopWorkTimeStamp))
                return new StopWorkTimeStamp(timestamp);

            return new NullTimeStamp();
        }

        private void NextElement(JsonReader reader)
        {
            if (!reader.Read())
                throw new InvalidOperationException();
        }
    }
}
