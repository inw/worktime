﻿using System;
using System.IO;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Windows.Storage;
using WorkTime.DataAccess.DTO;
using WorkTime.Domain.CrossCuttingConcerns;
using WorkTime.Domain.DTO;
using WorkTime.Domain.Repository;

namespace WorkTime.DataAccess
{
    public class DiskWorkDayRepository : IWorkDayRepository, IDiskStorage
    {
        private readonly StorageFolder _folder;
        private readonly ITimeProvider _clock;

        public DiskWorkDayRepository(ITimeProvider clock)
        {
            _folder = ApplicationData.Current.LocalFolder;
            _clock = clock;
        }

        public string StoragePath
            => _folder.Path;

        public async Task Delete(IWorkDayData model)
        {
            throw new NotImplementedException();
        }

        public async Task<IWorkDayData> GetByDate(DateTimeOffset date)
        {
            var fileName = CalculateFileName(date);
            try
            {
                WorkDayData result = await ReadFromFile(fileName);
                return result;
            }
            catch (FileNotFoundException)
            {
                return new NullWorkDayData();
            }
        }

        public async Task<IWorkDayData> GetCurrent()
        {
            var fileName = CalculateFileName(_clock.Now);
            try
            {
                WorkDayData result = await ReadFromFile(fileName);
                return result;
            }
            catch (FileNotFoundException)
            {
                return new WorkDayData(_clock.Now.Date);
            }
        }

        private async Task<WorkDayData> ReadFromFile(string fileName)
        {
            StorageFile storageFile = await _folder.GetFileAsync(fileName);
            var content = await FileIO.ReadTextAsync(storageFile);
            var result = JsonConvert.DeserializeObject<WorkDayData>(content);
            return result;
        }

        public async Task Insert(IWorkDayData model)
        {
            var filename = CalculateFileName(model.LastTimeStamp.Timestamp);
            var data = new WorkDayData(model);
            StorageFile currentFile = await _folder.CreateFileAsync(filename, CreationCollisionOption.ReplaceExisting);
            var content = JsonConvert.SerializeObject(data, Formatting.Indented);
            await FileIO.WriteTextAsync(currentFile, content);
        }

        private string CalculateFileName(DateTimeOffset timestamp)
        {
            return timestamp.ToString("yyyy-MM-dd") + ".json";
        }
    }
}
