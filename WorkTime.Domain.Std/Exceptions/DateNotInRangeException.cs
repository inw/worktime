﻿using System;
using System.Runtime.Serialization;

namespace WorkTime.Domain.Exceptions
{
    [Serializable]
    public class DateNotInRangeException : Exception
    {
        public DateNotInRangeException() { }
        public DateNotInRangeException(string message) : base(message) { }
        public DateNotInRangeException(string message, Exception inner) : base(message, inner) { }
        protected DateNotInRangeException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
