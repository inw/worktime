﻿using System;
using System.Runtime.Serialization;

namespace WorkTime.Domain.Exceptions
{
    [Serializable]
    public class InvalidTrackingDateException : Exception
    {
        public InvalidTrackingDateException()
        {
        }

        public InvalidTrackingDateException(string message) : base(message)
        {
        }

        public InvalidTrackingDateException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected InvalidTrackingDateException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
