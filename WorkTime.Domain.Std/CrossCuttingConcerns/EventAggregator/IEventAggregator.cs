﻿using System;

namespace WorkTime.Domain.CrossCuttingConcerns.EventAggregator
{
    public interface IEventAggregator
    {
        void Publish<T>(T message) where T : IApplicationMessage;
        void Subscribe<T>(Action<T> action) where T : IApplicationMessage;
        void Unsubscribe<T>(Action<T> action) where T : IApplicationMessage;
    }
}
