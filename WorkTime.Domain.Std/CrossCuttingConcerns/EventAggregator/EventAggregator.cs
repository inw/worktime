﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace WorkTime.Domain.CrossCuttingConcerns.EventAggregator
{
    public sealed class EventAggregator : IEventAggregator
    {
        private readonly ConcurrentDictionary<Type, List<WeakReference<object>>> subscriptions = new ConcurrentDictionary<Type, List<WeakReference<object>>>();
        private readonly object _listlock = new object();

        public void Publish<T>(T message) where T : IApplicationMessage
        {
            List<WeakReference<object>> subscribers;
            if (subscriptions.TryGetValue(typeof(T), out subscribers))
            {
                // To Array creates a copy in case someone unsubscribes in their own handler
                foreach (var subscriber in subscribers.ToArray())
                {
                    if (subscriber.TryGetTarget(out object target))
                        ((Action<T>)target)(message);
                    else
                        lock (_listlock)
                            subscribers.Remove(subscriber);
                }
            }
        }

        public void Subscribe<T>(Action<T> action) where T : IApplicationMessage
        {
            var subscribers = subscriptions.GetOrAdd(typeof(T), t => new List<WeakReference<object>>());

            lock (_listlock)
                subscribers.Add(new WeakReference<object>(action));
        }

        public void Unsubscribe<T>(Action<T> action) where T : IApplicationMessage
        {
            List<WeakReference<object>> subscribers;
            if (subscriptions.TryGetValue(typeof(T), out subscribers))
            {
                foreach (var subscriber in subscribers.ToArray())
                {
                    if (subscriber.TryGetTarget(out object target)
                        && target.Equals(action))
                        lock (_listlock)
                            subscribers.Remove(subscriber);
                }
            }
        }
    }
}

