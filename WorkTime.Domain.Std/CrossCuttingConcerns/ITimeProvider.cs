﻿namespace WorkTime.Domain.CrossCuttingConcerns
{
    public interface ITimeProvider
    {
        System.DateTimeOffset Now { get; }
    }
}