﻿using System;
using System.Threading.Tasks;

namespace WorkTime.Domain.CrossCuttingConcerns.Sync
{
    public interface IErrorHandler
    {
        Task HandleException(Exception ex);
    }
}
