﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace WorkTime.Domain.CrossCuttingConcerns.Sync
{
    public static partial class AsyncHelpers
    {
        /// <summary>
        /// Execute's an async Task<T> method which has a void return value synchronously
        /// </summary>
        /// <param name="task">Task<T> method to execute</param>
        public static void RunSync(Func<Task> task)
        {
            var oldContext = SynchronizationContext.Current;
            var synch = new ExclusiveSynchronizationContext();
            SynchronizationContext.SetSynchronizationContext(synch);
            synch.Post(async _ =>
            {
                try
                {
                    await task();
                }
                catch (Exception e)
                {
                    synch.InnerException = e;
                    throw;
                }
                finally
                {
                    synch.EndMessageLoop();
                }
            }, null);
            synch.BeginMessageLoop();

            SynchronizationContext.SetSynchronizationContext(oldContext);
        }

        /// <summary>
        /// Execute's an async Task<T> method which has a T return type synchronously
        /// </summary>
        /// <typeparam name="T">Return Type</typeparam>
        /// <param name="task">Task<T> method to execute</param>
        /// <returns></returns>
        public static T RunSync<T>(Func<Task<T>> task)
        {
            var oldContext = SynchronizationContext.Current;
            var synch = new ExclusiveSynchronizationContext();
            SynchronizationContext.SetSynchronizationContext(synch);
            T ret = default(T);
            synch.Post(async _ =>
            {
                try
                {
                    ret = await task();
                }
                catch (Exception e)
                {
                    synch.InnerException = e;
                    throw;
                }
                finally
                {
                    synch.EndMessageLoop();
                }
            }, null);
            synch.BeginMessageLoop();
            SynchronizationContext.SetSynchronizationContext(oldContext);
            return ret;
        }

#pragma warning disable S3168 // "async" methods should not return "void"
        public static async void AsAsyncVoidWithExceptionHandler(this Task task, IErrorHandler handler = null)
#pragma warning restore S3168 // "async" methods should not return "void"
        {
            try
            {
                await task;
            }
            catch (Exception ex)
            {
                if(handler != null)
                    await handler.HandleException(ex);
            }
        }

        public static async Task InvokeAsync(Func<Task> delegateFunction)
        {
            if (delegateFunction == null)
                await Task.CompletedTask;
            else
                await delegateFunction.Invoke();
        }

    }
}
