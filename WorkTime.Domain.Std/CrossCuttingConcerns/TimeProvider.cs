﻿using System;

namespace WorkTime.Domain.CrossCuttingConcerns
{
    public class TimeProvider : ITimeProvider
    {
        public DateTimeOffset Now
                => DateTimeOffset.Now;

        public TimeProvider()
        {

        }
    }
}
