﻿using System;
using System.Collections.Generic;
using WorkTime.Domain.DTO;
using WorkTime.Domain.Exceptions;

namespace WorkTime.Domain.WorkWeek
{
    public class CurrentWorkWeekModel : IWorkWeekModel
    {
        public DateTimeOffset WeekStart { get; }
        public Dictionary<DayOfWeek, TimeSpan> LoggedWorkByDay { get; }
        public CurrentWorkWeekModel(DateTimeOffset dateInCurrentWeek, DayOfWeek startOfWeek)
        {
            LoggedWorkByDay = new Dictionary<DayOfWeek, TimeSpan>();

            //DayOfWeek starts with sunday = 0
            var dayDiff = dateInCurrentWeek.DayOfWeek - startOfWeek;
            if (dayDiff == 0)
                WeekStart = dateInCurrentWeek.Date;
            else if (dayDiff > 0)
                WeekStart = dateInCurrentWeek.AddDays(-1 * dayDiff).Date;
            else if (dayDiff < 0)
                WeekStart = dateInCurrentWeek.AddDays(-1 * dayDiff - 7).Date;
        }

        public void Add(IWorkDaySummary dayToAdd)
        {
            if (LoggedWorkByDay.ContainsKey(dayToAdd.WeekDay))
                throw new DataAlreadyExistsException("There is already data set for this weekday, did you mean to UPDATE it?");

            if (dayToAdd.Date >= WeekStart && dayToAdd.Date < WeekStart.AddDays(7))
                LoggedWorkByDay.Add(dayToAdd.WeekDay, dayToAdd.LoggedWork);
            else
                throw new DateNotInRangeException("The day to add was not in the calendar week this instance was set to.");
        }

        public void AddOrUpdate(IWorkDaySummary dayToUpdate)
        {
            if (LoggedWorkByDay.ContainsKey(dayToUpdate.WeekDay))
                LoggedWorkByDay.Remove(dayToUpdate.WeekDay);

            Add(dayToUpdate);
        }
    }
}
