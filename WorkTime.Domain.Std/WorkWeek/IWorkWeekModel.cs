﻿using System;
using System.Collections.Generic;

namespace WorkTime.Domain.WorkWeek
{
    public interface IWorkWeekModel
    {
        Dictionary<DayOfWeek, TimeSpan> LoggedWorkByDay { get; }
        DateTimeOffset WeekStart { get; }
    }
}