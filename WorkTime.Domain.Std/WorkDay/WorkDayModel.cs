﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using WorkTime.Domain.DTO;
using WorkTime.Domain.Exceptions;

namespace WorkTime.Domain.WorkDay
{
    public class WorkDayModel : IWorkDayData, IWorkDayModel, IWorkDaySummary
    {
        private readonly List<DateTimeOffset> _startTimeStamps;
        private readonly List<DateTimeOffset> _stopTimeStamps;

        public TimeSpan LoggedWork { get; private set; }
        public IEnumerable<DateTimeOffset> WorkStartTimeStamps { get => _startTimeStamps; }
        public IEnumerable<DateTimeOffset> WorkStopTimeStamps { get => _stopTimeStamps; }
        public IWorkTimeStamp LastTimeStamp { get; private set; }
        public DateTimeOffset Date { get; private set; }
        public bool CurrentlyWorking { get; private set; }

        public DayOfWeek WeekDay => Date.DayOfWeek;

        public WorkDayModel(DateTimeOffset trackingDate)
        {
            _startTimeStamps = new List<DateTimeOffset>();
            _stopTimeStamps = new List<DateTimeOffset>();
            Date = trackingDate.Date;
            LoggedWork = new TimeSpan();
        }

        public WorkDayModel(IWorkDayData workDayData)
        {
            _startTimeStamps = workDayData.WorkStartTimeStamps?.ToList() ?? new List<DateTimeOffset>();
            _stopTimeStamps = workDayData.WorkStopTimeStamps?.ToList() ?? new List<DateTimeOffset>();
            LastTimeStamp = workDayData.LastTimeStamp;
            Date = workDayData.Date;
            LoggedWork = workDayData.LoggedWork;
            CurrentlyWorking = workDayData.CurrentlyWorking;
        }

        public IWorkDayData AddTimeStamp(IWorkTimeStamp timestamp)
        {
            if (!timestamp.Timestamp.Date.Equals(Date.Date))
                throw new InvalidTrackingDateException("Try and create another WorkDayModel. Every model is only valid for one calendar day");

            ProcessTimestamp(timestamp);

            LastTimeStamp = timestamp;
            return new WorkDayModel(this);
        }

        private void ProcessTimestamp(IWorkTimeStamp timestamp)
        {
            if (LastTimeStamp == null)
                LastTimeStamp = new NullTimeStamp();            

            if (timestamp is StartWorkTimeStamp)
            {
                _startTimeStamps.Add(timestamp.Timestamp);
                CurrentlyWorking = true;
            }
            else if (timestamp is StopWorkTimeStamp)
            {
                if (CurrentlyWorking && LastTimeStamp is NullTimeStamp)
                    LastTimeStamp = new StartWorkTimeStamp(_startTimeStamps.LastOrDefault());

                _stopTimeStamps.Add(timestamp.Timestamp);
                CurrentlyWorking = false;
                if (timestamp.Timestamp != default 
                    && LastTimeStamp is StartWorkTimeStamp 
                    && LastTimeStamp.Timestamp != default)
                {
                    LoggedWork = LoggedWork.Add(timestamp.Timestamp - LastTimeStamp.Timestamp);
                }
            }
            else
                throw new InvalidOperationException("Unsuported timestamp.");            
        }
    }
}
