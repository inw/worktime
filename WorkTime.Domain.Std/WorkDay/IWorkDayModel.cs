﻿using WorkTime.Domain.DTO;

namespace WorkTime.Domain.WorkDay
{
    public interface IWorkDayModel
    {
        IWorkDayData AddTimeStamp(IWorkTimeStamp timestamp);
    }
}
