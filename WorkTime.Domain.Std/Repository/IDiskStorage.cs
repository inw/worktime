﻿namespace WorkTime.Domain.Repository
{
    public interface IDiskStorage
    {
        string StoragePath { get; }
    }
}
