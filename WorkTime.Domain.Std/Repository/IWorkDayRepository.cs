﻿using System;
using System.Threading.Tasks;
using WorkTime.Domain.DTO;

namespace WorkTime.Domain.Repository
{
    public interface IWorkDayRepository
    {
        Task<IWorkDayData> GetCurrent();
        Task<IWorkDayData> GetByDate(DateTimeOffset date);
        Task Insert(IWorkDayData model);
        Task Delete(IWorkDayData model);
    }
}
