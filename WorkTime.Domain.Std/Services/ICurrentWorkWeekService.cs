﻿using System.Threading.Tasks;
using WorkTime.Domain.WorkWeek;

namespace WorkTime.Domain.Services
{
    public interface ICurrentWorkWeekService
    {
        Task<IWorkWeekModel> GetCurrentWeek();
    }
}
