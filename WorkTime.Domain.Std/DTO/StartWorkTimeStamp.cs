﻿using System;

namespace WorkTime.Domain.DTO
{
    public class StartWorkTimeStamp : IWorkTimeStamp
    {
        public DateTimeOffset Timestamp { get; }

        public StartWorkTimeStamp(DateTimeOffset timestamp)
        {
            Timestamp = timestamp;
        }
    }
}
