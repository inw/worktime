﻿using System;


namespace WorkTime.Domain.DTO
{
    public class StopWorkTimeStamp : IWorkTimeStamp
    {
        public DateTimeOffset Timestamp { get; }

        public StopWorkTimeStamp(DateTimeOffset timestamp)
        {
            Timestamp = timestamp;
        }
    }
}
