﻿using System;
using System.Collections.Generic;

namespace WorkTime.Domain.DTO
{
    public interface IWorkDayData
    {
        bool CurrentlyWorking { get; }
        IWorkTimeStamp LastTimeStamp { get; }
        IEnumerable<DateTimeOffset> WorkStartTimeStamps { get; }
        IEnumerable<DateTimeOffset> WorkStopTimeStamps { get; }
        DateTimeOffset Date { get; }
        TimeSpan LoggedWork { get; }
    }
}
