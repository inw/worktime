﻿using System;

namespace WorkTime.Domain.DTO
{
    public class WorkDaySummary : IWorkDaySummary
    {
        public DateTimeOffset Date { get; }
        public System.DayOfWeek WeekDay { get => Date.DayOfWeek; }
        public TimeSpan LoggedWork { get; }

        public WorkDaySummary(DateTimeOffset date, TimeSpan loggedWork)
        {
            Date = date;
            LoggedWork = loggedWork;
        }
    }
}
