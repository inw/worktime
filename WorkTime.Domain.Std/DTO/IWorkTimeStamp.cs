﻿using System;

namespace WorkTime.Domain.DTO
{
    public interface IWorkTimeStamp
    {
        DateTimeOffset Timestamp { get; }
    }
}
