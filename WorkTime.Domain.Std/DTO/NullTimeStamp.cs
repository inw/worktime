﻿using System;
using WorkTime.Domain.DTO;

namespace WorkTime.Domain.WorkDay
{
    public class NullTimeStamp : IWorkTimeStamp
    {
        public DateTimeOffset Timestamp => default;
    }
}
