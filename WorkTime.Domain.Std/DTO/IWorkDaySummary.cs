﻿namespace WorkTime.Domain.DTO
{
    public interface IWorkDaySummary
    {
        System.DateTimeOffset Date { get; }
        System.DayOfWeek WeekDay { get; }
        System.TimeSpan LoggedWork { get; }
    }
}
