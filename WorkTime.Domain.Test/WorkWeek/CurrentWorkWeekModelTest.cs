﻿using System;
using System.Globalization;
using FluentAssertions;
using WorkTime.Domain.DTO;
using WorkTime.Domain.Exceptions;
using WorkTime.Domain.WorkWeek;
using Xunit;

namespace WorkTime.Domain.Test.WorkWeek
{
    public class CurrentWorkWeekModelTest
    {
        private const string DATE_FORMAT = "yyyy-MM-dd";

        [Theory]
        [InlineData("2019-06-17", "2019-06-17", DayOfWeek.Monday)]
        [InlineData("2019-06-23", "2019-06-17", DayOfWeek.Monday)]
        [InlineData("2019-06-24", "2019-06-24", DayOfWeek.Monday)]

        [InlineData("2019-06-16", "2019-06-16", DayOfWeek.Sunday)]
        [InlineData("2019-06-17", "2019-06-16", DayOfWeek.Sunday)]
        [InlineData("2019-06-22", "2019-06-16", DayOfWeek.Sunday)]
        [InlineData("2019-06-23", "2019-06-23", DayOfWeek.Sunday)]
        public void Construction_WeekDayStart_IsInitializedToCorrectSartDateOfWeek(
            string dateToSet,
            string expectedStartDate,
            DayOfWeek weekStartDay)
        {
            var date = ParseTestDataDate(dateToSet);
            var expectedDate = ParseTestDataDate(expectedStartDate);

            var sut = new CurrentWorkWeekModel(date, weekStartDay);

            sut.WeekStart.Should().Be(expectedDate);
        }

        [Fact]
        public void AddingDates_DayIsInCurrentWeek_DayAdded()
        {
            var weekStart = ParseTestDataDate("2019-06-17");
            var dayToAdd = new WorkDaySummary(ParseTestDataDate("2019-06-17"), TimeSpan.FromHours(8));
            var sut = new CurrentWorkWeekModel(weekStart, weekStart.DayOfWeek);

            sut.Add(dayToAdd);

            sut.LoggedWorkByDay[dayToAdd.WeekDay].Should().Be(dayToAdd.LoggedWork);
        }

        [Fact]
        public void AddingDates_DayIsNotInCurrentWeek_ThrowsException()
        {
            var weekStart = ParseTestDataDate("2019-06-17");
            var dayToAdd = new WorkDaySummary(ParseTestDataDate("2019-06-25"), TimeSpan.FromHours(8));
            var sut = new CurrentWorkWeekModel(weekStart, weekStart.DayOfWeek);

            Action act = () => sut.Add(dayToAdd);

            act.Should().ThrowExactly<DateNotInRangeException>();
        }
        [Fact]
        public void AddingDates_DayWasAlreadyAdded_ThrowsException()
        {
            var weekStart = ParseTestDataDate("2019-06-17");
            var dayToAdd = new WorkDaySummary(ParseTestDataDate("2019-06-18"), TimeSpan.FromHours(8));
            var sut = new CurrentWorkWeekModel(weekStart, weekStart.DayOfWeek);

            sut.Add(dayToAdd);
            Action act = () => sut.Add(dayToAdd);

            act.Should().ThrowExactly<DataAlreadyExistsException>();
        }

        [Fact]
        public void AddOrUpdateDate_DayIsInCurrentWeek_DayAdded()
        {
            var weekStart = ParseTestDataDate("2019-06-17");
            var dayToAdd = new WorkDaySummary(ParseTestDataDate("2019-06-17"), TimeSpan.FromHours(8));
            var sut = new CurrentWorkWeekModel(weekStart, weekStart.DayOfWeek);

            sut.AddOrUpdate(dayToAdd);

            sut.LoggedWorkByDay[dayToAdd.WeekDay].Should().Be(dayToAdd.LoggedWork);
        }

        [Fact]
        public void AddOrUpdateDate_DayIsNotInCurrentWeek_ThrowsException()
        {
            var weekStart = ParseTestDataDate("2019-06-17");
            var dayToAdd = new WorkDaySummary(ParseTestDataDate("2019-06-25"), TimeSpan.FromHours(8));
            var sut = new CurrentWorkWeekModel(weekStart, weekStart.DayOfWeek);

            Action act = () => sut.AddOrUpdate(dayToAdd);

            act.Should().ThrowExactly<DateNotInRangeException>();
        }

        [Fact]
        public void AddOrUpdateDate_DayWasAlreadyAdded_DataIsUpdated()
        {
            var weekStart = ParseTestDataDate("2019-06-17");
            var dayToAdd = new WorkDaySummary(ParseTestDataDate("2019-06-18"), TimeSpan.FromHours(8));
            var dayToUpdate = new WorkDaySummary(ParseTestDataDate("2019-06-18"), TimeSpan.FromHours(9));
            var sut = new CurrentWorkWeekModel(weekStart, weekStart.DayOfWeek);

            sut.Add(dayToAdd);
            sut.AddOrUpdate(dayToUpdate);

            sut.LoggedWorkByDay[dayToAdd.WeekDay].Should().Be(dayToUpdate.LoggedWork);
        }

        private DateTimeOffset ParseTestDataDate(string date)
        {
            var provider = CultureInfo.InvariantCulture;
            return new DateTimeOffset(DateTime.ParseExact(date, DATE_FORMAT, provider));
        }


    }
}


